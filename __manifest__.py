# -*- coding: utf-8 -*-

{
    'name': 'Le Filament - Maintenance',
    'version': '10.0.1.0.0',
    'summary': 'Gestion des contrats de maintenance',
    'license': 'AGPL-3',
    'description': """
    MODULE DE GESTION DE CONTRATS DE MAINTENANCE
    Ajoute un menu au module Projet :
   - List View des contrats
   - Filtres sur contrats en cours
   - FormView des contrats
    """,
    'author': 'LE FILAMENT',
    'category': 'LE FILAMENT',
    'depends': ['base', 'project'],
    'contributors': [
        'Benjamin Rivier <benjamin@le-filament.com>',
    ],
    'website': 'http://www.le-filament.com',
    'data': [
        'views/lefilament_maintenance_view.xml',
        'views/schedule.xml',
        'security/ir.model.access.csv',
        'data/mail_subtype.xml',
    ],
}
