# -*- coding: utf-8 -*-

# © 2017 Le Filament (<https://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from datetime import datetime, timedelta
from odoo import models, fields, api
from odoo.exceptions import UserError


class LeFilamentMaintenance(models.Model):
    _name = 'project.maintenance'
    _description = 'Suivi des programmes de maintenance'
    _inherit = 'mail.message.subtype', 'mail.thread'

    name = fields.Char('Description', required=True,)

    lf_m_user_id = fields.Many2one('res.users', string='Responsable Contrat',)

    lf_m_partner_id = fields.Many2one('res.partner', string='Client',)
    lf_m_project_id = fields.Many2one('project.project', string='Projet',)

    lf_m_date_debut = fields.Date('Date de début')
    lf_m_date_fin = fields.Date('Date de Fin')
    lf_m_montant = fields.Float('Montant')

    lf_m_termine = fields.Boolean('Terminé')
    lf_m_renouvelle = fields.Boolean('À renouveller')

    lf_m_task = fields.Char('Action')
    lf_m_cycle = fields.Integer('Récurrence (jours)', default=0,)
    lf_m_date_task = fields.Datetime('Prochaine échéance')
    lf_m_comment = fields.Char('Commentaire')

    lf_m_note = fields.Text('Notes')

    subtype_id = fields.Many2one('mail.message.subtype',
                                 string='Message Subtype',
                                 required=True,
                                 ondelete='cascade')

    @api.one
    def action_ok(self):
        if self.lf_m_cycle != 0 and self.lf_m_date_task:
            next_date = datetime.strptime(
                self.lf_m_date_task, '%Y-%m-%d %H:%M:%S') + timedelta(
                    days=self.lf_m_cycle)
            body = '<li>Prochaine action le ' \
                + next_date.strftime('%d/%m/%Y')+'</li>'
            if self.lf_m_task:
                body += '<li>Action : '+self.lf_m_task+'</li>'
            if self.lf_m_comment:
                body += '<li>Commentaire : '+self.lf_m_comment+'</li>'
            body_html = "<div><b>%(title)s</b> : %(action)s</div>\
                </ul>%(body)s</ul>" % {
                    'title': self.name,
                    'action': 'Action OK',
                    'body': body,
                    'comment': self.lf_m_comment or '',
                    }
            self.message_post(body_html,
                              subject='Activité OK',
                              subtype_id=self.env.ref(
                                  "lefilament_maintenance.\
                                      mail_message_subtype_maintenance").id)
            self.lf_m_date_task = next_date
            self.lf_m_comment = ""
        else:
            raise UserError("La date de prochaine échéance et la récurrence \
                            doivent être renseignés")

    @api.multi
    def action_check(self):
        projets = self.env['project.maintenance'].search([])
        for record in projets:
            if record.lf_m_date_task:
                action_date = datetime.strptime(record.lf_m_date_task,
                                                '%Y-%m-%d %H:%M:%S')
                if datetime.now() > action_date:
                    act_date = action_date.strftime('%d/%m/%Y').encode('utf-8')
                    body_html = '<div style="color:#fc8675;">\
                        <b>%(title)s</b></div>Projet : <b>%(projet)s</b>\
                        <br />Action à effectuer le \
                        <span style="color:#fc8675;font-weight: 700;">\
                        %(next)s</span><br />' % {
                            'title': 'Retard Maintenance',
                            'projet': record.name.encode('utf-8'),
                            'next': act_date
                            }
                    record.message_post(
                        body_html,
                        subject='Retard Maintenance',
                        subtype_id=self.env.ref(
                            "lefilament_maintenance.\
                                mail_message_subtype_maintenance_check").id)
